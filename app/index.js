import React, { Component } from 'react';
import { render } from 'react-dom';

import './index.css';
import './components/Taskfield';
import App from './components/App';
import { Provider } from 'react-redux';
import { createStore } from 'redux';
import reducer from './reducers/index';


const reminderStore = createStore(reducer);

render(<Provider store={reminderStore}><App /></Provider>, document.getElementById('root'));