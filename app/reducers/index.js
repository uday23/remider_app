import { ADD_REMIDER, DELETE_REMINDER, CLEAR_REMINDER } from '../constant';
import { bake_cookie, read_cookie} from 'sfcookies';

const reminder = (action) => {
  let { text, dueDate } = action;
  return {
    id: Math.random(),
    text,
    dueDate
  }
}

const removeById = (state = [], id) => {
   const reminders = state.filter( reminder => reminder.id != id);
   console.log('reminder list after delete', reminders);
   return reminders;
}

const reminders = (state = [], action) => {
  state = read_cookie('reminderList');
  let reminderList = [...state];
  switch (action.type) {
    case ADD_REMIDER:
      reminderList = [...state, reminder(action)];
      bake_cookie('reminderList', reminderList);
      // console.log('reminders as state', reminderList);
      return reminderList;
    case DELETE_REMINDER:
       reminderList = removeById(state, action.id);
       bake_cookie('reminderList', reminderList);
       return reminderList;
    case CLEAR_REMINDER: 
       reminderList = [];
       bake_cookie('reminderList', reminderList);
       return reminderList;
    default:
      return state;
  }

}

export default reminders;
