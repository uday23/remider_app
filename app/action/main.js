import { ADD_REMIDER, DELETE_REMINDER, CLEAR_REMINDER } from '../constant';


export const addReminder = (text, dueDate) => {
   const action = {
   	type: ADD_REMIDER,
   	text,
   	dueDate
   }
   console.log('add reminder text',  action);
   console.log()
   return action;
}


export const deleteReminder = (id) => {
  const action ={ 
  	type: DELETE_REMINDER,
  	id
  }
  console.log('delete reminder', action);
  return action;
}

export const clearAllReminders = () => {
	const action = {
		type : CLEAR_REMINDER
	}

	return action;
}