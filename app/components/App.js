import React, { Component } from 'react';
import { connect } from 'react-redux';
// import { bindActionCreators } from 'redux';
import { addReminder, deleteReminder, clearAllReminders } from '../action/main';
import moment from 'moment';


class App extends Component {

  constructor(props) {
    super(props);
    this.state = {
      text: '',
      dueDate: ''
    }
  }

  addToReminderList() {
    console.log('button click event output: ', this)
    let actionInReminderList = this.props.reminderList.findIndex(item => item.text === this.state.text);
    let dateString = new Date(this.state.dueDate);
    if (actionInReminderList === -1) {
      this.props.addReminder(this.state.text, dateString);
    }
    this.refs.reminderInput.value = '';
  }

  deleteThis(id) {
    this.props.deleteReminder(id)
  }

  renderReminders() {
    const { reminderList } = this.props;
    return (
      <ul className="list-container">
           {
           	reminderList.map(reminder => { 
           		return ( 
       			       <li key={reminder.id} className="list-group-item reminder-item">
       			             <span className="list-item">{reminder.text}</span>
       			             <span className="list-item pull-right cross-sign" onClick={() => this.deleteThis(reminder.id)}>&#x2715;</span>
       			             <br />
                             <span className="time-string">{moment(reminder.dueDate).fromNow()}</span>
       			        </li> 
           			) 
           	})
           }
  		</ul>
    )
  }

  render() {
    return (
      <div className="container">
         <br/><br/>
         <div className="app-title">Remider</div>
         <br />
         <div className="forn-inline text-center">
            <div className="form-group">              
                  <input className="form-control reminder-text-field" placeholder="please add your reminder here. . ." 
                          ref="reminderInput" onChange = {event => this.setState({text : event.target.value})} />
                          <br />   
                  <input type="datetime-local" className="form-control reminder-text-field" onChange = {event => this.setState({dueDate : event.target.value})} /> 
                          <br/>           
                  <button className="btn btn-success btn-outline add-reminder-btn" onClick={() => this.addToReminderList()}>Add Reminder</button>           	
            </div>         	
         </div>
         <div className="reminder-list">
         	{this.renderReminders()}

         </div>  	
         <div className="text-center">
            { this.props.reminderList.length !== 0 ? <button className="btn btn-sm btn-danger" onClick={() => this.props.clearAllReminders() }>Delete All</button> : '' }
         </div>
      </div>
    )
  }
}


function mapStateToProps(state) {
  console.log('inside mapStateToProps: ', state);
  return {
    reminderList: state
  }
}

// function mapDispatchToProps(dispatch){
//    return bindActionCreators({addReminder}, dispatch);
// }

// shortcut for above method which dispatch the siugle action to component
export default connect(mapStateToProps, { addReminder, deleteReminder, clearAllReminders })(App);
